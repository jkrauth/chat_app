import sys
import socket
import selectors
import types
from threading import Thread
import tkinter
from time import sleep

HEADER_LENGTH = 10

IP = '127.0.0.1'
PORT = 1234

sel = selectors.DefaultSelector()

def start_connection(ip, port):
    print('Welcome to the chatroom!')
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setblocking(False)
    sock.connect_ex((ip, port))
    #print(f'Server at {IP}:{PORT} not found!\nExit chatroom...')
    events = selectors.EVENT_READ | selectors.EVENT_WRITE
    data = types.SimpleNamespace(
        message=msg_welcome,
        outb=b'',
    )
    sel.register(sock, events, data=data)
    
def service_connection(key, mask):
    sock = key.fileobj
    data = key.data
    global textfield

    if mask & selectors.EVENT_READ:
        #print('in read block')
        recv_data = sock.recv(HEADER_LENGTH)
        if recv_data:
            message_length = int(recv_data.decode('utf-8').strip())
            message = sock.recv(message_length).decode('utf-8')
            msg_list.insert(tkinter.END, message)
            msg_list.yview(tkinter.END)
            print("received message from.. ")
        else:
            print('Closing connection..')
            sel.close()
            top.destroy()            

    if mask & selectors.EVENT_WRITE:
        #print('in write block')
        #print(f'---\n{data.message}\n{data.outb}\n{textfield}\n---')
        if data.message:
            data.outb = data.message.encode('utf-8')
            data.message = b''
        if not data.outb and textfield:
            message = f'{my_username} > {textfield}'
            textfield = ''
            # Post on timeline
            msg_list.insert(tkinter.END, message)
            msg_list.yview(tkinter.END)
            # Get ready for sending
            data.outb = message.encode('utf-8')
        if data.outb:
            print("sending", repr(data.outb), "to connection..")
            header = f'{len(data.outb):<{HEADER_LENGTH}}'.encode('utf-8')
            sock.send(header + data.outb)
            #data.outb = data.outb[sent:]
            data.outb = b''


def send(event=None):
    global textfield
    message = my_msg.get()
    if message:
        textfield = message 
        my_msg.set("")
    
def loop(event=None):
    try:
        while True:
            sleep(0.1)
            events = sel.select(timeout=1)
            if events:
                for key, mask in events:
                    service_connection(key, mask)
            if not sel.get_map():
                break
    except KeyboardInterrupt:
        print('caught keyboard interrupt, exiting')
    except ValueError:
        print('connection closed')
    finally:
        sel.close()

                    
def on_closing(event=None):
    """This function is to be called when the window is closed."""
    sel.close()
    top.destroy()            
    print(f'Chat client closed! Goodbye, {my_username}!')

#username = my_username.encode('utf-8')
#username_header = f'{len(username):<{HEADER_LENGTH}}'.encode('utf-8')
#client_socket.sendall(username_header + username)

my_username = input('Enter your username: ')

msg_welcome = f'{my_username} joined the chat'
textfield = ''

start_connection(IP, PORT)

#====== Create TkInter window ======#
top = tkinter.Tk()
top.title(f'Chatroom: {my_username}')

messages_frame = tkinter.Frame(top)

# For the messages to be sent.
my_msg = tkinter.StringVar() 

# To navigate through past messages
scrollbar = tkinter.Scrollbar(messages_frame) 

msg_list = tkinter.Listbox(messages_frame, height=15, width=60,
                           yscrollcommand=scrollbar.set)

scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)
msg_list.pack(side=tkinter.LEFT, fill=tkinter.BOTH)
msg_list.pack()
messages_frame.pack()

entry_field = tkinter.Entry(top, textvariable=my_msg, width=60)
entry_field.bind('<Return>', send)
entry_field.pack()

top.protocol('WM_DELETE_WINDOW', on_closing)


socket_thread = Thread(target=loop)
socket_thread.start()
tkinter.mainloop() # Starts GUI execution
socket_thread.join()

