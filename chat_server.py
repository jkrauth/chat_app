#!/usr/bin/env python
import socket
import selectors
import types
from time import sleep

HEADER_LENGTH = 10

IP = '127.0.0.1' # On local PC only
#IP = '0.0.0.0' # Open for network
PORT = 1234

sel = selectors.DefaultSelector()

# Create socket using ipv4 and TCP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Avoid bind() exception: OSError: [Errno 48] Address already in use
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind((IP, PORT))
server_socket.listen()
print(f'Listening for connections on {IP}:{PORT}...')
server_socket.setblocking(False)
sel.register(server_socket, selectors.EVENT_READ, data=None)

def service_connection(key, mask):
    sock = key.fileobj
    data = key.data
    if mask & selectors.EVENT_READ:
        message_header = sock.recv(HEADER_LENGTH)
        # If False, client disconnected, cleanup
        if message_header:
            message_length = int(message_header.decode('utf-8').strip())
            message_content =  sock.recv(message_length)
            data.outb += message_header + message_content
        else:
            print('closing connection to ', data.addr)
            sel.unregister(sock)
            sock.close()

    if mask & selectors.EVENT_WRITE:
        if data.outb:
            # Broadcast message
            message_sent = False
            for client in sel.get_map().values():
                client_socket = client.fileobj
                # But don't send it to sender or server
                if client_socket != server_socket and client_socket != sock:
                    sent = client_socket.send(data.outb)
                    message_sent = True
            if message_sent:
                data.outb = data.outb[sent:]
    
def accept_wrapper(sock):
    conn, addr = key.fileobj.accept()
    print('Accepted connection from ', addr)
    conn.setblocking(False)
    # Add accepted socket to select.select() list
    data = types.SimpleNamespace(addr=addr, inb=b'', outb=b'')
    # We want to read and write 
    events = selectors.EVENT_READ | selectors.EVENT_WRITE
    sel.register(conn, events, data=data)    


try:
    while True:
        events = sel.select(timeout=None)
        for key, mask in events:
            sleep(0.1)
            if key.data is None:
                accept_wrapper(key.fileobj)                
            else:
                service_connection(key, mask)


except KeyboardInterrupt:
    print('\nClosing server!')
finally:
    sel.close()

